import React from 'react';
import classnames from 'classnames';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: <>Search Engine Optimization</>,
    imageUrl: 'img/undraw_docusaurus_mountain.svg',
    description: (
      <>
       The best way to make a site profitable is by increasing visitors. And when increasing visitors organically is the most profitable strategy, it does not require a lot of work.
      </>
    ),
  },
  {
    title: <>Forensic SEO</>,
    imageUrl: 'img/undraw_docusaurus_tree.svg',
    description: (
      <>
        orensic SEO is a practice which is designed to identify and eliminate potential performance problems in a website to determine what could be causing the lost of organic traffic.
      </>
    ),
  },
  {
    title: <>SEO Penalty Recovering</>,
    imageUrl: 'img/undraw_docusaurus_react.svg',
    description: (
      <>
        When a website falls under penalty of SEO, the website may be removed from Google search results entirely. But it does not mean it is gone for good. We will help your websites regain its rankings.
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={classnames('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Search Engine Optimization ${siteConfig.title}`}
      description="We work with brands all around the world by implementing SEO strategies to bring their brands to new levels of engagement. We're here to help!.<head />">
      <header className={classnames('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={classnames(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/doc1')}>
              Rank Your Site!
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
